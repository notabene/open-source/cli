import keytar from "keytar";

const getCredentials = async () => {
  const credentials = await keytar.getPassword("notabene", "cli");

  if (credentials === null) {
    throw new Error("You must login first");
  }

  const [clientId, clientSecret] = credentials.split(":");

  return {
    clientId,
    clientSecret,
  };
};

export default getCredentials;
