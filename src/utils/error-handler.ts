import { printError, printErrorObject } from "./output";

export default async (message: string, error: any): Promise<never> => {
  if (error?.response) {
    printErrorObject(error.response.data);
  } else if (error?.request) {
    printErrorObject(error.request);
  } else if (error) {
    printError(`Error: ${error.message}`);
  } else {
    printError(message);
  }

  process.exit(1);
};
