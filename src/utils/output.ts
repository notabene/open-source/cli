import colors from "colors/safe";
import prettyjson from "prettyjson";
import { EOL } from "node:os";

export const printErrorObject = (data: any) => {
  process.stderr.write(
    EOL + colors.red(prettyjson.render(data, { noColor: true })) + EOL + EOL
  );
};

export const printError = (data: string) => {
  process.stderr.write(EOL + colors.red(data) + EOL + EOL);
};

export const printSuccess = (data: any) => {
  process.stdout.write(
    EOL + colors.green(prettyjson.render(data, { noColor: true })) + EOL + EOL
  );
};
