#!/usr/bin/env node

import yargs from "yargs";
import { hideBin } from "yargs/helpers";
import errorHandler from "./utils/error-handler";
import * as authLogin from "./commands/auth/login";
import * as authLogout from "./commands/auth/logout";
import * as authToken from "./commands/auth/token";
import * as keysCreate from "./commands/keys/create";

const cli = yargs(hideBin(process.argv))
  .scriptName("notabene")
  .version("0.1.0")
  .usage("Welcome to the Notabene CLI")
  .command(authLogin)
  .command(authLogout)
  .command(authToken)
  .command(keysCreate)
  .command(
    "$0",
    "",
    () => undefined,
    () => {
      yargs.showHelp();
    }
  )
  .strict()
  .alias({ h: "help" })
  .fail(errorHandler)
  .epilogue(
    "For more information on how to send and receive Travel Rule transactions using Notabene check out https://devx.notabene.id"
  );

cli.argv;
