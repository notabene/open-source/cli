import { getToken } from "@notabene/nodejs";
import getCredentials from "../../utils/get-credentials";
import { printSuccess } from "../../utils/output";

export const command = "auth:token";

export const desc = "Get authentication token";

export const handler = async () => {
  const { clientId, clientSecret } = await getCredentials();

  const token = await getToken(clientId, clientSecret);

  printSuccess(`Token: ${token}`);
};
