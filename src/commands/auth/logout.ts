import keytar from "keytar";
import { printSuccess } from "../../utils/output";

export const command = "auth:logout";

export const desc = "Logout from the Notabene API";

export const handler = async (argv: any) => {
  await keytar.deletePassword("notabene", "cli");

  printSuccess("Successfully logged out");
};
