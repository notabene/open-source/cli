import { getToken } from "@notabene/nodejs";
import keytar from "keytar";
import { CommandBuilder } from "yargs";
import { printSuccess } from "../../utils/output";

export const command = "auth:login";

export const desc = "Login to the Notabene API";

export const builder: CommandBuilder = (args) => {
  return args
    .option("clientId", {
      describe: "API Client ID",
      requiresArg: true,
    })
    .option("clientSecret", {
      describe: "API Client Secret",
      requiresArg: true,
    })
    .option("env", {
      describe: "Notabene environment (test or prod)",
      requiresArg: true,
      default: "prod",
    })
    .demandOption(["clientId", "clientSecret"], "Use --help to see more info");
};

export const handler = async (argv: any) => {
  await keytar.setPassword(
    "notabene",
    "cli",
    `${argv.clientId}:${argv.clientSecret}`
  );

  const token = await getToken(
    argv.clientId,
    argv.clientSecret,
    "https://auth.notabene.id",
    `https://api.notabene.${argv.env === "prod" ? "id" : "dev"}`
  );

  printSuccess("Successfully logged in");
};
