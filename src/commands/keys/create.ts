import NotabeneKeyToolset from "@notabene/pii-sdk";
import { randomBytes } from "crypto";
import { CommandBuilder } from "yargs";
import { printSuccess } from "../../utils/output";

export const command = "keys:create";

export const desc = "Create a private/public key pair for encrypting PII";

export const builder: CommandBuilder = (args) => {
  return args
    .option("notabeneURL", {
      describe: "URL of the Notabene API",
      default: "https://api.notabene.id",
      requiresArg: false,
    })
    .option("secret", {
      describe: "KMS secret",
      default:
        "7630350f98daef5f3cf121f952d1c03c3ee828d19ecba3975213f8d309d20665",
      requiresArg: false,
    })
    .option("escrowURL", {
      describe: "URL of the Notabene PII Escrow",
      default: "https://pii.notabene.id",
      requiresArg: false,
    })
    .option("infuraProjectId", {
      describe: "ID of the Infura project for resolving DIDs",
      default: "314417ca139c499982fd7d8e4450a89a",
      requiresArg: false,
    });
};

export const handler = async (argv: any) => {
  const keytoolset = new NotabeneKeyToolset({
    kmsSecretKey: argv.secret,
    piiURL: argv.escrowURL,
  });

  const key = await keytoolset.createKey();

  printSuccess(key);
};
