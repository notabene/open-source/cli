<div align="center">

<img src="https://assets-global.website-files.com/5e68f0772de982756aa8c1a4/5eee5fb470215e6ecdc34b94_Full_transparent_black_1280x413.svg" height=50>

<br>

# Notabene CLI

[![pipeline status](https://gitlab.com/notabene/open-source/cli/badges/master/pipeline.svg)](https://gitlab.com/notabene/open-source/cli/-/commits/master)
[![Latest Release](https://gitlab.com/notabene/open-source/cli/-/badges/release.svg)](https://gitlab.com/notabene/open-source/cli/-/releases)

Notabene CLI for managing tokens and keys.

[Documentation](https://devx.notabene.id) •
[Installation](#installation)

</div>

## Installation

Install the library globally using Yarn:

```
yarn global add @notabene/cli
```

or NPM:

```
npm i -g @notabene/cli
```

Make sure that the path to globally installed packages are in your `$PATH` environment variable.

## Tokens

### Login

Login using your Notabene issued Client ID and Client Secret:

```bash
notabene auth:login --clientId={CLIENT_ID} --clientSecret={CLIENT_SECRET}
```

### Logout

Logout from Notabene:

```bash
notabene auth:logout
```

### Token

Generate a M2M token for use with the Notabene Travel Rule gateway. Note you must be logged in first:

```bash
notabene auth:token
```

## Keys

### Create a key

You can use the CLI to generate a key that can be used to encrypt PII information to be sent as part of a Travel Rule message:

```
notabene keys:create
```

This will generate a JSON object containing an [Ed25519](https://en.wikipedia.org/wiki/EdDSA) key and metadata which can be passed to the [Notabene SDK](https://gitlab.com/notabene/open-source/notabene-nodejs) when creating transactions to encrypt the PII.

## [License](LICENSE.md)

BSD 3-Clause © Notabene Inc.
